package com.zmex.plugin.s3;

public enum BucketNameState {
    FREE,
    EXISTS_OWNED,
    NAME_USED
}

package com.zmex.plugin.s3;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.HeadBucketRequest;

public class S3Client {

    private final AmazonS3 s3Client;

    public S3Client(String region, String accesskey, String secretKey) {
        AWSCredentialsProvider creds = new AWSStaticCredentialsProvider(new BasicAWSCredentials(accesskey, secretKey));
        this.s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(creds)
                .withRegion(region).build();
    }

    public void createBucket(String bucketName) {
        BucketNameState bucketState = checkBucketName(bucketName);
        switch (bucketState) {
            case FREE:
                s3Client.createBucket(bucketName);
                break;
            case EXISTS_OWNED:
                return;
            case NAME_USED:
                throw new RuntimeException("Name " + bucketName + " already in use");
        }
    }


    private BucketNameState checkBucketName(String bucketName) {
        HeadBucketRequest request = new HeadBucketRequest(bucketName);
        try {
            s3Client.headBucket(request);
            return BucketNameState.EXISTS_OWNED;
        } catch (AmazonServiceException ex) {

            switch (ex.getStatusCode()) {
                case 404:
                    return BucketNameState.FREE;
                case 403:
                case 301:
                    return BucketNameState.NAME_USED;
                default:
                    throw new RuntimeException(ex);
            }
        }
    }
}

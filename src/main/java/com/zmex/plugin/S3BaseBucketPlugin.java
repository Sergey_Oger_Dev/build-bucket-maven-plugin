package com.zmex.plugin;

import com.zmex.plugin.s3.S3Client;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;


@Mojo(name = "create-bucket", defaultPhase = LifecyclePhase.DEPLOY)
public class S3BaseBucketPlugin extends AbstractMojo {

    @Parameter(required = true, name = "bucketName")
    private String bucketName;

    @Parameter(required = true, name = "region")
    private String region;

    @Parameter(required = true, name = "accessKey")
    private String accessKey;

    @Parameter(required = true, name = "secretKey")
    private String secretKey;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            final S3Client client = new S3Client(region, accessKey, secretKey);
            client.createBucket(bucketName);
        } catch (RuntimeException ex) {
            throw new MojoExecutionException(ex.getMessage());
        }
    }
}
